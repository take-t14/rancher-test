http://c.itdo.jp/technical-information/docker-kubernetes/ha-rancher/

# 構成
lb-01
OS Rocky Linux8
vcpu 1
memory 1024MB
IP 192.168.0.150
HDD 40GB

【amd-1】rancher-master-01
OS Rocky Linux8
vcpu 4
memory 16384MB
IP 192.168.0.130/24
HDD 40GB

【mac】rancher-master-02
OS Rocky Linux8
vcpu 6
memory 8192MB
IP 192.168.0.151/24
HDD 40GB

【vaio】rancher-worker-01
OS Rocky Linux8
vcpu 4
memory 4096MB
IP 192.168.0.140/24
HDD 40GB

【hp】rancher-worker-02
OS Rocky Linux8
vcpu 8
memory 8192MB
IP 192.168.0.153/24
HDD 40GB


# vagrant構築
vagrant plugin install vagrant-disksize
vagrant plugin install vagrant-vbguest

cd /Users/tadanobu/Documents/Kubernetes/rancher-ha/lb-01
vagrant up

cd /Users/tadanobu/Documents/Kubernetes/rancher-ha/rancher-master
vagrant up

cd /Users/tadanobu/Documents/Kubernetes/rancher-ha/rancher-worker
vagrant up

※vagrant up後、全てvagrant haltしてからVirtualBoxの設定→ネットワーク→アダプター2→アダプタータイプを「PCnet-FAST III (Am79C973)」へ変更して保存する。

# 全vagrantの端末へvagrant shhで接続し、以下を設定する
[tadanobu@MacBook-Pro ]$ vagrant ssh rancher-master-03
[rancher@rancher-master-01 ~]$ sudo passwd
※パスワードは以下
ffff

[rancher@rancher-master-01 ~]$ su
[root@rancher-master-01 rancher]# vi /etc/ssh/sshd_config
PermitRootLogin yes
PasswordAuthentication yes
[root@rancher-master-01 rancher]# vi /etc/selinux/config
# SELINUX=enforcing
SELINUX=disabled
[root@rancher-master-01 rancher]# systemctl disable firewalld
[root@rancher-master-01 rancher]# systemctl stop firewalld
[root@rancher-master-01 rancher]# timedatectl set-timezone Asia/Tokyo

[root@rancher-master-01 rancher]# vi /etc/rc.d/rc.local
## ※ファイル末尾に以下を追記(Vagrant特有の設定)
```
echo -e "nameserver 192.168.0.150\nnameserver 8.8.8.8\nnameserver 8.8.4.4" > /etc/resolv.conf
```
[root@rancher-master-01 rancher]# chmod 744 /etc/rc.d/rc.local
[root@rancher-master-01 rancher]# vi /etc/NetworkManager/NetworkManager.conf
## ※以下のように[main]セクション以下へ「dns=none」を追記する(Vagrant特有の設定)
```
[main]
dns=none
```
[root@rancher-master-01 rancher]# exit
[rancher@rancher-master-01 ~]$ exit
[tadanobu@MacBook-Pro ]$ vagrant halt rancher-master-01
[tadanobu@MacBook-Pro ]$ vagrant up rancher-master-01

# LB構築
[tadanobu@MacBook-Pro ]$ cd /Users/tadanobu/Documents/Kubernetes/rancher-ha/lb-01
[tadanobu@MacBook-Pro ]$ vagrant ssh
[vagrant@lb-01 ~]$ su
[root@lb-01 vagrant]# dnf -y install nginx nginx-mod-stream
cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf_back
vi /etc/nginx/nginx.conf
## ※ファイルの内容 デフォルトの記載は消して、以下を書き込みます。
```
user nginx;
worker_processes 4;
worker_rlimit_nofile 40000;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 8192;
}

stream {
    upstream backend {
        least_conn;
        server 192.168.0.130:9345 max_fails=3 fail_timeout=5s;
        server 192.168.0.140:9345 max_fails=3 fail_timeout=5s;
        server 192.168.0.151:9345 max_fails=3 fail_timeout=5s;
        server 192.168.0.153:9345 max_fails=3 fail_timeout=5s;
    }

    # This server accepts all traffic to port 9345 and passes it to the upstream. 
    # Notice that the upstream name and the proxy_pass need to match.
    server {
        listen 9345;
        proxy_pass backend;
    }
    upstream rancher_api {
        least_conn;
        server 192.168.0.130:6443 max_fails=3 fail_timeout=5s;
        server 192.168.0.140:6443 max_fails=3 fail_timeout=5s;
        server 192.168.0.151:6443 max_fails=3 fail_timeout=5s;
        server 192.168.0.153:6443 max_fails=3 fail_timeout=5s;
    }
    server {
        listen     6443;
        proxy_pass rancher_api;
    }
    upstream rancher_http {
        least_conn;
        server 192.168.0.130:80 max_fails=3 fail_timeout=5s;
        server 192.168.0.140:80 max_fails=3 fail_timeout=5s;
        server 192.168.0.151:80 max_fails=3 fail_timeout=5s;
        server 192.168.0.153:80 max_fails=3 fail_timeout=5s;
    }
    server {
        listen     80;
        proxy_pass rancher_http;
    }
    upstream rancher_https {
        least_conn;
        server 192.168.0.130:443 max_fails=3 fail_timeout=5s;
        server 192.168.0.140:443 max_fails=3 fail_timeout=5s;
        server 192.168.0.151:443 max_fails=3 fail_timeout=5s;
        server 192.168.0.153:443 max_fails=3 fail_timeout=5s;
    }
    server {
        listen     443;
        proxy_pass rancher_https;
    }
}
```

[root@lb-01 vagrant]# systemctl enable --now nginx

# DNS構築
[root@lb-01 vagrant]# dnf -y install bind bind-utils
[root@lb-01 vagrant]# vi /etc/named.example.com.conf
## ファイル内容
```
zone example.com. IN {
  type master;
  file "example.com.zone";
};
```

[root@lb-01 vagrant]# cp /etc/named.conf /etc/named.conf_back
[root@lb-01 vagrant]# vi /etc/named.conf

## ※options内のlisten-on port 53を、以下内容にする
```
listen-on port 53 { localhost; 8.8.8.8; 8.8.4.4; };
```

## ※options内のallow-queryを、以下内容にする
```
allow-query     { localhost; 192.168.0.0/24; 8.8.8.8/32; } 
```

## ※options内のdnssec-〜を、以下内容にする
```
//dnssec-enable yes;
//dnssec-validation yes;
dnssec-enable no;
dnssec-validation no;
```

## ※options内へ、以下内容を追記する
```
    forwarders { 8.8.8.8; 8.8.4.4; };
```

## ※以下を末尾へ追記
```
include "/etc/named.example.com.conf";
```


## ゾーンファイル作成
[root@lb-01 vagrant]# vi /var/named/example.com.zone
### ファイル内容（タブはタブキーで入力すること）
```
$TTL 86400

@ IN SOA lb.example.com root.example.com (
    2018050600
    3600
    900
    604800
    86400
)

    IN  NS  lb.example.com.
lb  IN  A   192.168.0.150
rancher-master-01   IN  A   192.168.0.130
rancher-master-02   IN  A   192.168.0.151
rancher-worker-01   IN  A   192.168.0.140
rancher-worker-02   IN  A   192.168.0.153
```

### bind再起動
[root@lb-01 vagrant]# systemctl enable --now named

### mysql-syellインストール（mysqlテスト用）
[root@lb-01 vagrant]# dnf localinstall https://dev.mysql.com/get/mysql80-community-release-el8-1.noarch.rpm
[root@lb-01 vagrant]# rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
[root@lb-01 vagrant]# dnf install mysql-shell -y

### 全てのVMのDNSサーバをlb-0に設定（「rancher-〜」のVM全てに以下の設定をする）

[tadanobu@MacBook-Pro ]$ vagrant ssh rancher-master-01
[rancher@rancher-master-01 ~]$ su
[root@rancher-master-01 ~]# cp /etc/resolv.conf /etc/resolv.conf_back
[root@rancher-master-01 ~]# vi /etc/resolv.conf

## 内容を以下に書き換える
```
nameserver 192.168.0.150
options edns0
search example.com
```

# Dockerのインストール
[tadanobu@MacBook-Pro ]$ vagrant ssh rancher-master-01
[rancher@rancher-master-01 ~]$ su
[root@rancher-master-01 ~]# dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
[root@rancher-master-01 ~]# dnf repolist | grep docker
[root@rancher-master-01 ~]# dnf -y install docker-ce
※エラーが出る場合は以下
[root@rancher-master-01 ~]# dnf -y install docker-ce --allowerasing
[root@rancher-master-01 ~]# docker -v
[root@rancher-master-01 ~]# systemctl enable docker; systemctl start docker
[root@rancher-master-01 ~]# su vagrant
[rancher@rancher-master-01 ~]$ sudo usermod -aG docker ${USER}
[rancher@rancher-master-01 ~]$ exit
[root@rancher-master-01 ~]# systemctl restart docker
[root@rancher-master-01 ~]# docker info
[root@rancher-master-01 ~]# docker run hello-world
[root@rancher-master-01 ~]# docker rm $(docker ps -q -a)
[root@rancher-master-01 ~]# docker rmi $(docker images -q)
[root@rancher-master-01 ~]# dnf -y install net-tools bind-utils
※longhornの為にiscsi-initiator-utilsをインストール
[root@rancher-master-01 ~]# dnf install -y iscsi-initiator-utils nfs-utils
[root@rancher-master-01 ~]# dnf install -y epel-release
[root@rancher-master-01 ~]# dnf install -y htop

# _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# _/ rancher-master-01 rke2-serverインストール

https://devopstales.github.io/home/rke2-calico/
https://docs.rke2.io/install/methods/
https://www.suse.com/suse-rancher/support-matrix/all-supported-versions

[root@rancher-master-01 /]# cat << EOF > /etc/yum.repos.d/rancher-rke2-1-26-latest.repo
[rancher-rke2-common-latest]
name=Rancher RKE2 Common Latest
baseurl=https://rpm.rancher.io/rke2/latest/common/centos/8/noarch
enabled=1
gpgcheck=1
gpgkey=https://rpm.rancher.io/public.key

[rancher-rke2-1-26-latest]
name=Rancher RKE2 1.26 Latest
baseurl=https://rpm.rancher.io/rke2/latest/1.26/centos/8/x86_64
enabled=1
gpgcheck=1
gpgkey=https://rpm.rancher.io/public.key
EOF
dnf -y install rke2-server;


[root@rancher-master-01 /]# mkdir -p /etc/rancher/rke2; cat << EOF >  /etc/rancher/rke2/config.yaml
advertise-address: 192.168.0.130
node-ip: 192.168.0.130
node-external-ip: 192.168.0.130
EOF
cni:
  - calico
disable:
  - rke2-canal
  - rke2-kube-proxy
EOF

[root@rancher-master-01 /]# mkdir -p /var/lib/rancher/rke2/server/manifests; cat << EOF >  /var/lib/rancher/rke2/server/manifests/rke2-canal-config.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-canal
  namespace: kube-system
spec:
  valuesContent: |-
    flannel:
      iface: "enp2s0"
EOF
spec:
  valuesContent: |-
    calico:
      felixIptablesBackend: nft
EOF

[root@rancher-master-01 /]# systemctl enable rke2-server --now
[root@rancher-master-01 /]# systemctl status rke2-server
[root@rancher-master-01 /]# mkdir ~/.kube
[root@rancher-master-01 /]# ln -s /etc/rancher/rke2/rke2.yaml ~/.kube/config

[root@rancher-master-01 /]# dnf install -y git
[root@rancher-master-01 /]# git clone https://github.com/ahmetb/kubectx /opt/kubectx
[root@rancher-master-01 /]# ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
[root@rancher-master-01 /]# ln -s /opt/kubectx/kubens /usr/local/bin/kubens
[root@rancher-master-01 /]# echo 'PATH=$PATH:/usr/local/bin' >> /etc/profile
[root@rancher-master-01 /]# echo 'PATH=$PATH:/var/lib/rancher/rke2/bin' >> /etc/profile
[root@rancher-master-01 /]# source /etc/profile
[root@rancher-master-01 /]# curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
[root@rancher-master-01 /]# helm version
[root@rancher-master-01 /]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces
[root@rancher-master-01 /]# cat /var/lib/rancher/rke2/server/token
※表示されたtokenを/etc/rancher/rke2/config.yamlへ反映する。
[root@rancher-master-01 /]# cat ~/.kube/config



# _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# _/ rancher-master-02 rke2-serverインストール

https://docs.rke2.io/install/ha/  

[root@rancher-master-02 /]# curl -sfL https://get.rke2.io | sh -

※以下token: の右には上記「[root@rancher-master-01 /]# cat /var/lib/rancher/rke2/server/token」で表示された内容を記載
[root@rancher-master-02 /]# mkdir -p /etc/rancher/rke2; cat << EOF > /etc/rancher/rke2/config.yaml
advertise-address: 192.168.0.151
node-ip: 192.168.0.151
node-external-ip: 192.168.0.151
EOF
cni:
  - calico
disable:
  - rke2-canal
  - rke2-kube-proxy
EOF

[root@rancher-master-02 /]# mkdir -p /var/lib/rancher/rke2/server/manifests; cat << EOF >  /var/lib/rancher/rke2/server/manifests/rke2-canal-config.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-canal
  namespace: kube-system
spec:
  valuesContent: |-
    flannel:
      iface: "ens33"
EOF
  valuesContent: |-
    calico:
      felixIptablesBackend: nft

[root@rancher-master-02 /]# systemctl enable rke2-server --now
[root@rancher-master-02 /]# systemctl status rke2-server
[root@rancher-master-02 /]# cat /var/lib/rancher/rke2/server/node-token  
[root@rancher-master-02 /]# mkdir ~/.kube
[root@rancher-master-02 /]# ln -s /etc/rancher/rke2/rke2.yaml ~/.kube/config
[root@rancher-master-02 /]# dnf install -y git
[root@rancher-master-02 /]# git clone https://github.com/ahmetb/kubectx /opt/kubectx
[root@rancher-master-02 /]# ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
[root@rancher-master-02 /]# ln -s /opt/kubectx/kubens /usr/local/bin/kubens
[root@rancher-master-02 /]# echo 'PATH=$PATH:/usr/local/bin' >> /etc/profile
[root@rancher-master-02 /]# echo 'PATH=$PATH:/var/lib/rancher/rke2/bin' >> /etc/profile
[root@rancher-master-02 /]# source /etc/profile
[root@rancher-master-02 /]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces




# _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# _/ rancher-wordker-01 rke2-agentインストール

[root@rancher-worker-01 /]# cat << EOF > /etc/yum.repos.d/rancher-rke2-1-26-latest.repo
[rancher-rke2-common-latest]
name=Rancher RKE2 Common Latest
baseurl=https://rpm.rancher.io/rke2/latest/common/centos/8/noarch
enabled=1
gpgcheck=1
gpgkey=https://rpm.rancher.io/public.key

[rancher-rke2-1-26-latest]
name=Rancher RKE2 1.26 Latest
baseurl=https://rpm.rancher.io/rke2/latest/1.26/centos/8/x86_64
enabled=1
gpgcheck=1
gpgkey=https://rpm.rancher.io/public.key
EOF

[root@rancher-worker-01 /]# dnf -y install rke2-agent

※以下token: の右には上記「[root@rancher-master-01 /]# cat /var/lib/rancher/rke2/server/token」で表示された内容を記載
[root@rancher-worker-01 /]# mkdir -p /etc/rancher/rke2; cat << EOF > /etc/rancher/rke2/config.yaml
server: https://lb.example.com:9345
token: K103a0bdfeadf1814f072516d10bf7875043f5e0b75e36e9de139b5d5256c7120e5::server:510df30375b6f3d27c1869e042de6d35
advertise-address: 192.168.0.140
node-ip: 192.168.0.140
node-external-ip: 192.168.0.140
EOF
cni:
  - calico
disable:
  - rke2-canal
  - rke2-kube-proxy
EOF

[root@rancher-worker-01 /]# mkdir -p /var/lib/rancher/rke2/server/manifests; cat << EOF >  /var/lib/rancher/rke2/server/manifests/rke2-canal-config.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-canal
  namespace: kube-system
spec:
  valuesContent: |-
    flannel:
      iface: "wlp2s0"
EOF
  valuesContent: |-
    calico:
      felixIptablesBackend: nft

[root@rancher-worker-01 /]# systemctl enable rke2-agent --now
[root@rancher-worker-01 /]# systemctl status rke2-agent
[root@rancher-worker-01 /]# mkdir ~/.kube
[root@rancher-worker-01 /]# vi ~/.kube/config
```
※上記[root@rancher-master-01 /]# cat ~/.kube/configで表示されたものを記載する。
```

[root@rancher-worker-01 /]# dnf install -y git
[root@rancher-worker-01 /]# git clone https://github.com/ahmetb/kubectx /opt/kubectx
[root@rancher-worker-01 /]# ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
[root@rancher-worker-01 /]# ln -s /opt/kubectx/kubens /usr/local/bin/kubens
[root@rancher-worker-01 /]# echo 'PATH=$PATH:/usr/local/bin' >> /etc/profile
[root@rancher-worker-01 /]# echo 'PATH=$PATH:/var/lib/rancher/rke2/bin' >> /etc/profile
[root@rancher-worker-01 /]# source /etc/profile
[root@rancher-worker-01 /]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces





# _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# _/ rancher-wordker-02 rke2-agentインストール

[root@rancher-worker-02 /]# curl -sfL https://get.rke2.io | sh -

※以下token: の右には上記「[root@rancher-master-01 /]# cat /var/lib/rancher/rke2/server/token」で表示された内容を記載
[root@rancher-worker-02 /]# mkdir -p /etc/rancher/rke2; cat << EOF > /etc/rancher/rke2/config.yaml
server: https://lb.example.com:9345
token: K103a0bdfeadf1814f072516d10bf7875043f5e0b75e36e9de139b5d5256c7120e5::server:510df30375b6f3d27c1869e042de6d35
advertise-address: 192.168.0.153
node-ip: 192.168.0.153
node-external-ip: 192.168.0.153
EOF
cni:
  - calico
disable:
  - rke2-canal
  - rke2-kube-proxy
EOF

[root@rancher-worker-02 /]# mkdir -p /var/lib/rancher/rke2/server/manifests; cat << EOF >  /var/lib/rancher/rke2/server/manifests/rke2-canal-config.yaml
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-canal
  namespace: kube-system
spec:
  valuesContent: |-
    flannel:
      iface: "ens33"
EOF
spec:
  valuesContent: |-
    calico:
      felixIptablesBackend: nft
EOF

[root@rancher-master-02 /]# systemctl enable rke2-server --now
[root@rancher-master-02 /]# systemctl status rke2-server
[root@rancher-master-02 /]# cat /var/lib/rancher/rke2/server/node-token  
[root@rancher-master-02 /]# mkdir ~/.kube
[root@rancher-master-02 /]# ln -s /etc/rancher/rke2/rke2.yaml ~/.kube/config
[root@rancher-master-02 /]# dnf install -y git
[root@rancher-master-02 /]# git clone https://github.com/ahmetb/kubectx /opt/kubectx
[root@rancher-master-02 /]# ln -s /opt/kubectx/kubectx /usr/local/bin/kubectx
[root@rancher-master-02 /]# ln -s /opt/kubectx/kubens /usr/local/bin/kubens
[root@rancher-master-02 /]# echo 'PATH=$PATH:/usr/local/bin' >> /etc/profile
[root@rancher-master-02 /]# echo 'PATH=$PATH:/var/lib/rancher/rke2/bin' >> /etc/profile
[root@rancher-master-02 /]# source /etc/profile
[root@rancher-master-02 /]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces







root@lb-0:~# helm repo add stable https://charts.helm.sh/stable
root@lb-0:~# helm repo update
root@lb-0:~# helm repo list
root@lb-0:~# helm repo add rancher-latest https://releases.rancher.com/server-charts/latest

https://cert-manager.io/docs/installation/
https://cert-manager.io/docs/installation/helm/
root@lb-0:~# helm repo add jetstack https://charts.jetstack.io
root@lb-0:~# helm repo update

# cert-managerインストール
# root@lb-0:~# helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.7.1 --set installCRDs=true
root@lb-0:~# helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.12.3 --set installCRDs=true --set webhook.timeoutSeconds=30
NAME: cert-manager
LAST DEPLOYED: Sun Sep 24 13:12:11 2023
NAMESPACE: cert-manager
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
cert-manager v1.12.3 has been deployed successfully!

In order to begin issuing certificates, you will need to set up a ClusterIssuer
or Issuer resource (for example, by creating a 'letsencrypt-staging' issuer).

More information on the different types of issuers and how to configure them
can be found in our documentation:

https://cert-manager.io/docs/configuration/

For information on how to configure cert-manager to automatically provision
Certificates for Ingress resources, take a look at the `ingress-shim`
documentation:

https://cert-manager.io/docs/usage/ingress/


## ※エラーの場合
E0312 18:33:06.996301    2619 reflector.go:138] k8s.io/client-go@v0.23.1/tools/cache/reflector.go:167: Failed to watch *unstructured.Unstructured: Get "https://192.168.0.151:6443/apis/batch/v1/namespaces/cert-manager/jobs?allowWatchBookmarks=true&fieldSelector=metadata.name%3Dcert-manager-startupapicheck&resourceVersion=6921&timeoutSeconds=565&watch=true": http2: client connection lost
W0312 18:33:09.104210    2619 reflector.go:324] k8s.io/client-go@v0.23.1/tools/cache/reflector.go:167: failed to list *unstructured.Unstructured: Get "https://192.168.0.151:6443/apis/batch/v1/namespaces/cert-manager/jobs?fieldSelector=metadata.name%3Dcert-manager-startupapicheck&resourceVersion=6921": dial tcp 192.168.0.151:6443: connect: no route to host
E0312 18:33:09.104287    2619 reflector.go:138] k8s.io/client-go@v0.23.1/tools/cache/reflector.go:167: Failed to watch *unstructured.Unstructured: failed to list *unstructured.Unstructured: Get "https://192.168.0.151:6443/apis/batch/v1/namespaces/cert-manager/jobs?fieldSelector=metadata.name%3Dcert-manager-startupapicheck&resourceVersion=6921": dial tcp 192.168.0.151:6443: connect: no route to host
Error: INSTALLATION FAILED: failed post-install: timed out waiting for the condition
[root@lb-01 ~]# helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.7.1 --set installCRDs=true
Error: INSTALLATION FAILED: rendered manifests contain a resource that already exists. Unable to continue with install: could not get information about the resource ServiceAccount "cert-manager-cainjector" in namespace "cert-manager": etcdserver: request timed out
[root@lb-01 ~]# helm uninstall cert-manager jetstack/cert-manager --namespace cert-manager
Error: uninstall: Release not loaded: cert-manager: query: failed to query with labels: etcdserver: request timed out
[root@lb-01 ~]# helm list
Error: list: failed to list: etcdserver: request timed out

## helmのcert-managerをアンインストールして、namespaceも消してからリトライする
```
[root@lb-01 ~]# helm uninstall cert-manager --namespace cert-manager
[root@lb-01 ~]# kubectl delete namespace cert-manager
[root@lb-01 ~]# kubectl get ns cert-manager -o json > tmp.json
[root@lb-01 ~]# vi ./tmp.json
[root@lb-01 ~]# curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/cert-manager/finalize
```

root@lb-0:~# kubectl get pods --namespace cert-manager

# rancherインストール
https://www.rancher.co.jp/docs/rancher/v2.x/en/installation/ha/helm-rancher/
# root@lb-0:~# kubectl create namespace cattle-system
root@lb-0:~# helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
# root@lb-0:~# helm install rancher rancher-stable/rancher --version=2.6 --namespace cattle-system --set hostname=lb.example.com
# root@lb-0:~# helm install rancher rancher-stable/rancher --namespace cattle-system --set hostname=lb.example.com
# root@lb-0:~# helm install rancher rancher-stable/rancher --version=2.6 --namespace cattle-system --set hostname=lb.example.com --set replicas=1
root@lb-0:~# helm install rancher rancher-stable/rancher --version=2.7.5 --namespace cattle-system --create-namespace --set hostname=lb.example.com --set replicas=2 
NAME: rancher
LAST DEPLOYED: Sun Sep 24 13:13:44 2023
NAMESPACE: cattle-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Rancher Server has been installed.

NOTE: Rancher may take several minutes to fully initialize. Please standby while Certificates are being issued, Containers are started and the Ingress rule comes up.

Check out our docs at https://rancher.com/docs/

If you provided your own bootstrap password during installation, browse to https://lb.example.com to get started.

If this is the first time you installed Rancher, get started by running this command and clicking the URL it generates:

```
echo https://lb.example.com/dashboard/?setup=$(kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}')
```

To get just the bootstrap password on its own, run:

```
kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{ "\n" }}'
```


Happy Containering!
  
  
## 複数のノードでPodを動かす設定  
root@lb-0:~# helm get values rancher -n cattle-system > ~/current_values.yaml  
root@lb-0:~# vim ~/current_values.yaml  
```  
podTemplate:
  spec:
    affinity:
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: kubernetes.io/hostname
              operator: In
              values:
              - 'rancher-master-01'
              - 'rancher-master-02'
```  
root@lb-0:~# helm upgrade -f ~/current_values.yaml rancher rancher-stable/rancher --version=2.7.5 --namespace cattle-system --create-namespace --set hostname=lb.example.com --set replicas=2  
  
  
## ※ダメな場合
Error: INSTALLATION FAILED: Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": Post "https://ingress-nginx-controller-admission.ingress-nginx.svc:443/networking/v1/ingresses?timeout=10s": x509: certificate signed by unknown authority
Error: INSTALLATION FAILED: Internal error occurred: failed calling webhook "webhook.cert-manager.io": Post "https://cert-manager-webhook.cert-manager.svc:443/mutate?timeout=10s": context deadline exceeded

[root@lb-01 ~]# helm uninstall rancher rancher-stable/rancher --namespace cattle-system
W0313 18:06:19.316108    4751 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
W0313 18:06:25.113995    4751 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+

[root@lb-01 ~]# kubectl delete namespace cattle-system
[root@lb-01 ~]# kubectl get ns cattle-system -o json > tmp.json
[root@lb-01 ~]# vi tmp.json
[root@lb-01 ~]# curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/cattle-system/finalize


[root@lb-01 ~]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces



## ※rke2アンインストール
[root@rancher-master-01 /]# /usr/bin/rke2-uninstall.sh; reboot

[root@rancher-master-02 /]# /usr/local/bin/rke2-uninstall.sh; reboot


Rancherに初めてアクセスするようなので、起動時のパスワードを事前に設定している場合は、ここに入力してください。そうでなければ、ランダムなパスワードが生成されます。それを見つけるには

docker run」インストールの場合。
docker psでコンテナIDを検索し、実行します。
docker logs  container-id  2>&1 | grep "Bootstrap Password:"
を実行します。

Helmインストールの場合、実行します。

kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'
 を実行します。

bntsgl9n5rvhx5wsz6swncbpkpggcjd78plgv47hgrx8867bpvmknr
ユーザID(user id)：admin
パスワード(password) : ed1go9GVQsDHxhAU



































# 鍵作成＆配布
root@lb-0:~# ssh-keygen
root@lb-0:~# cat ~/.ssh/id_rsa.pub 
[vagrant@rancher-master-02 ~]$ su
[root@rancher-master-02 vagrant]# mkdir ~/.ssh
[root@rancher-master-02 vagrant]# vi ~/.ssh/authorized_keys
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDHsKXXEW65KrANhZrqO+C6rUjV99mV60hYPNJ2tL1U/heXi5wQJZZQe16Z9329kFJEUreetlh0V3jtOaisO2HIiqwhCPeiFTui7OfL0Aw4hPtZUV2vsdYlrA2Ya8pd1ps+DuKnV/VBYtajc9E5YpLmuj6SyYFwddvJ53Slf9ojnRQkYUwkqE78+Ip13aFcd4IQKi8j1QGdxC70f6H7c2rN91iigeFIyoEL2XAIApUPPWe6A2R/RgT6bHJvT+3yy52gAAclQ2/5l2UzYb3fKoY0qyuo3bKUtsGKSRGfPxXoIabmIGZOiGEJ6WPqBq3znOJzxJZQX8nKIDYdE+1s0z1WO16h4krjILmLU6KupS8tj2ISOjc5VNKTW+zvtBwsDSG+gW9u4F5gLSNXPwMwzA1ubb5eSSe6k4RNa33YmU6BWJMgSXY7kpLNlukN3iGkQ6tjqGBI4OeBmg1Id4nNJ7/AWb1oC1PuBYDkCsj6i/eDpK2QLf2Bg7L0MOCj9K58dWs= root@lb-01
```
[root@rancher-master-02 vagrant]# chmod 700 ~/.ssh; chmod 600 ~/.ssh/authorized_keys 

https://www.suse.com/suse-rancher/support-matrix/all-supported-versions/rancher-v2-6-0/


# k8sクラスタファイル作成
root@lb-0:~# vi rancher-cluster.yml
## ファイル内容
```
nodes:
  - address: 192.168.0.130
    user: root
    role: [controlplane, etcd, worker]
    internal_address: 192.168.0.130
  - address: 192.168.0.151
    user: root
    role: [worker]
    internal_address: 192.168.0.151
  - address: 192.168.0.153
    user: root
    role: [worker]
    internal_address: 192.168.0.153
  - address: 192.168.0.140
    user: root
    role: [worker]
    internal_address: 192.168.0.140

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h

network:
  plugin: calico
# network:
#   plugin: canal
#   options:
#     canal_iface: eth2

dns:
  provider: coredns
  upstreamnameservers:
  - 192.168.0.150

# Required for external TLS termination with
# ingress-nginx v0.22+
ingress:
  provider: nginx
  options:
    use-forwarded-headers: "true"
kubernetes_version: "v1.21.9-rancher1-1"
```

root@lb-0:~# rke up --config rancher-cluster.yml

## ※１）FATA[0218] Failed to get job complete status for job rke-ingress-controller-deploy-job in namespace kube-system
## ※２）FATA[0546] [controlPlane] Failed to bring up Control Plane: [Failed to verify healthcheck: Service [kube-apiserver] is not healthy on host [192.168.0.153]. Response code: [403], response body: {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"forbidden: User \"kube-apiserver\" cannot get path \"/healthz\"","reason":"Forbidden","details":{},"code":403}
## ※３）FATA[0175] Failed to get job complete status for job rke-coredns-addon-deploy-job in namespace kube-system
, log: I0313 01:17:47.077492       1 controller.go:611] quota admission added evaluator for: rolebindings.rbac.authorization.k8s.io]
１ノードだけ有効にして他コメント化した状態でrancher-cluster.ymlを編集してからrke up --config rancher-cluster.ymを実行。その後１ノードずつ増やしつつrke up --config rancher-cluster.ymを実行するとうまくいったりする。
また、rke up --config rancher-cluster.ymlをもう一度実行すると、何回かやっていると成功する場合がある。
https://github.com/rancher/rke/issues/1461


## ※エラーの場合、またはクラスター作り直しの時
https://github.com/rancher/rke/issues/1835
```
1.【スキップ】新しいservice-account-token-key.pemを生成します
root@lb-0:~# openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./cluster_certs/kube-service-account-token-key.pem -out ./cluster_certs/kube-service-account-token.pem

2.rkeクラスター削除
root@lb-0:~# rke remove --config rancher-cluster.yml; rm -rf ~/.kube

3.すべてのDockerコンテナとkubernetes証明書をクリーンアップ
# 全ノードで以下を実行
root@lb-0:~# ssh root@192.168.0.151
[root@rancher-master-01 ~]# docker stop $(docker ps -a -q); docker rm $(docker ps -a -q); docker rmi $(docker images -q); docker volume rm $(docker volume ls -q); for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done; rm -rf /etc/ceph /etc/cni /etc/kubernetes /opt/cni /opt/rke /run/secrets/kubernetes.io /run/calico /run/flannel /var/lib/calico /var/lib/etcd /var/lib/cni /var/lib/kubelet /var/lib/rancher/rke/log /var/log/containers /var/log/pods /var/run/calico; docker images; docker ps -a; systemctl restart docker; shutdown now -r

root@lb-0:~# for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done; rm -rf /etc/ceph /etc/cni /etc/kubernetes /opt/cni /opt/rke /run/secrets/kubernetes.io /run/calico /run/flannel /var/lib/calico /var/lib/etcd /var/lib/cni /var/lib/kubelet /var/lib/rancher/rke/log /var/log/containers /var/log/pods /var/run/calico

4.【スキップ】rkeクラスタ作成
rke up --config rancher-cluster.yml --custom-certs
```

root@lb-0:~# mkdir ~/.kube; cp kube_config_rancher-cluster.yml ~/.kube/config
root@lb-0:~# cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
root@lb-0:~# dnf repolist
root@lb-0:~# dnf -y install kubelet kubeadm kubectl
root@lb-0:~# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces
https://kubernetes.io/ja/docs/reference/kubectl/cheatsheet/

# lb-01へHelmのインストール
root@lb-0:~# curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
root@lb-0:~# helm version

psql -h 127.0.0.1 -p 5432 -U postgres -d postgres
psql -h 127.0.0.1 -p 5432 -U pgpool -d postgres
psql -h 192.168.0.130 -p 30000 -U postgres -d postgres
psql -h 127.0.0.1 -p 9999 -U postgres -d postgres
psql -h 127.0.0.1 -p 9999 -U pgpool -d postgres
psql -h 192.168.0.130 -p 30001 -U postgres -d postgres
psql -h 192.168.0.160 -p 9999 -U postgres -d postgres

[root@web-db-01 ~]# psql -h 192.168.0.160 -p 9999 -U postgres -d postgres
postgres=# SET password_encryption = 'md5';
postgres=# \password test_pg_user
[root@web-db-01 ~]# su - postgres
[root@web-db-01 ~]# pg_md5 --md5auth --username=test_pg_user ffff
[root@web-db-01 ~]# cat /etc/pgpool-II/pool_passwd


/opt/remi/php80/root/usr/bin/php8 artisan --env=dev cache:clear
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev config:clear
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev route:clear
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev view:clear
/opt/remi/php80/root/usr/bin/php8 /usr/local/bin/composer dump-autoload
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev clear-compiled
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev optimize;
/opt/remi/php80/root/usr/bin/php8 artisan --env=dev config:cache

psql -h 192.168.0.160 -p 9999 -U test_pg_user -d test_db


php8 artisan migrate:fresh
php8 artisan db:seed


# 旧マスターの復旧（セカンダリの設定参考）
https://weblog.hirohiro716.com/?p=784


# セカンダリ構築
su postgres
pg_basebackup -R -h 192.168.0.130 -p 30000 -U repl -D /var/lib/pgsql/14/data/ -P

supervisorctl stop postgresql
supervisorctl stop pgpool
rm -rf /var/lib/pgsql/data/*
rm -rf /var/lib/pgsql/data/.*
su postgres
pg_basebackup -R -h 192.168.0.160 -p 5432 -U repl -D /var/lib/pgsql/data/ -P
exit
supervisorctl start postgresql
supervisorctl start pgpool


PGPASSWORD=ffff gosu postgres /usr/pgsql-14/bin/pg_basebackup -D /var/lib/pgsql/data/ -U repl -h 192.168.0.160 -p 5432 -X stream -w -v -P

♯ フェールオーバー
su postgres
/usr/pgsql-14/bin/pg_ctl promote -D /var/lib/pgsql/14/data/


# カタログ追加

## rancherでProject、Namespaceを作成
Project : rancher-test-pj
Namespace : rancher-test-ns

## Longhoneインストール
Namespace : rancher-test-ns
longhorn:100.1.2+up1.2.4
Default Replica Count: 3 → 1
# Longhorn Default Settings: OFF → ON
# Guaranteed Engine Manager CPU: 12 → 3
# Guaranteed Replica Manager CPU: 12 → 3
Create Default Disk on Labeled Nodes : OFF → ON
node.longhorn.io/create-default-disk=true" のラベルを持つノードで、他にディスクが存在しない場合にのみデフォルトディスクを自動作成する。無効の場合、各ノードが最初に追加されたときに、すべての新しいノードでデフォルトディスクが作成されます。

Default Data Locality : disable
Longhornのボリュームは、そのボリュームを使用するPodと同じノードにボリュームのローカルレプリカが存在する場合、データローカリティがあると言います。この設定は、Longhorn UIからボリュームを作成する際のデフォルトのデータローカリティを指定します。Kubernetesの設定の場合は、StorageClassの`dataLocality`を更新します。 利用可能なモードは次のとおりです。- **disabled**. これはデフォルトのオプションです。アタッチされたボリューム（ワークロード）と同じノードにレプリカがある場合とない場合があります - **best-effort**. このオプションは、アタッチされたボリューム（ワークロード）と同じノード上にレプリカを維持しようとするようLonghornに指示します。十分なディスク容量がない、互換性のないディスクタグがあるなど、環境の制約によりアタッチされたボリューム（ワークロード）のローカルにレプリカを保持できない場合でも、Longhornはそのボリュームを停止しません。

Replica Auto Balance : best-effort
この設定を有効にすると、利用可能なノードが発見されたときに自動的にレプリカのリバランスが行われます。利用可能なグローバルオプションは以下の通りです。- 無効**。これはデフォルトのオプションです。レプリカの自動バランスは行われません。- 最小限の努力で**。このオプションは、最小限の冗長性を確保するためにレプリカのバランスをとるように指示します。- **best-effort**。このオプションは、冗長性が均等になるようにレプリカのバランスをとるように指示します。また、Longhornは個別のボリューム設定もサポートしています。この設定はvolume.spec.replicaAutoBalanceで指定でき、グローバルな設定より優先されます。ボリュームスペックのオプションは以下の通りです。- 無視される**。これはデフォルトのオプションで、Longhornにグローバルな設定を継承するように指示します。- 無効(disabled)**。このオプションは、レプリカの自動バランスを行わないように指示します。- **least-effort**. このオプションは、最小限の冗長性を確保するためにレプリカのバランスをとるように指示します。- **best-effort**. このオプションは、冗長性が均等になるようにレプリカのバランスをとるように指示します。

Replica Node Level Soft Anti-Affinity : OFF → ON
同じボリュームの健全なレプリカが存在するノードでのスケジューリングを許可します。デフォルトはfalseです。

Default Replica Count : 3 → 1
Longhorn UIからボリュームを作成する際のデフォルトのレプリカ数です。Kubernetesの設定の場合、StorageClassの`numberOfReplicas`を更新します。デフォルトでは3です。




## take-t14-rancher-catalog
Name : take-t14-rancher-catalog
URL : https://gitlab.com/take-t14/rancher-catalog.git
Type : git
Branch : main

## Zalando PostgreSQL Operator
Name : zalando-postgres-operator
URL : https://raw.githubusercontent.com/zalando/postgres-operator/master/charts/postgres-operator
Type : https

Name : zalando-postgres-operator-ui
URL : https://raw.githubusercontent.com/zalando/postgres-operator/master/charts/postgres-operator-ui
Type : https

## Elasticsearch
Name : elastic
URL : https://helm.elastic.co
Type : https


# カタログデプロイ

## Zalando PostgreSQL Operator
Namespace : rancher-test-ns
Name : zalando-postgres-operator
configKubernetes > cluster_name_label ：zalando-postgres-operator
postgres-operator : 1.7.1
connection_pooler_max_db_connections : 60 → 5
connection_pooler_number_of_instances : 2 → 1

## Zalando PostgreSQL Operator UI
Namespace : rancher-test-ns
Name : zalando-postgres-operator-ui
postgres-operator-ui :1.7.1
envs > operatorApiUrl : http://zalando-postgres-operator-ui
envs > operatorClusterNameLabel ：zalando-postgres-operator
envs > targetNamespace ："*"
ingress > enabled ：true
ingress > hosts > hosts ： zalando-postgres-operator-ui

## PostgreSQL Cluster作成
「http://zalando-postgres-operator-ui」へブラウザアクセス
※要hostsファイルへ「192.168.0.150 zalando-postgres-operator-ui」登録
Name : zalando-postgres-operator
Namespace : rancher-test-ns
Number of instances : 1
Users : test_pg_user
Databases : test_db > test_pg_user

mysql


## laravel-ddd-sample
username : test_pg_user
password : cO3kImdx7Dfoyw8lGdgdgvc0TJxIbPijB4sJEq7KSVViYqTnJ5VofNBK8yuxNsr1

## Wordpress

Name : shesselink81.github.io
URL : 	https://shesselink81.github.io/helm-charts/public-charts/
externalDatabase > database : bitnami_wordpress
externalDatabase > host : mysql-innodbcluster.mysql-operator
externalDatabase > password : 'ffff'
externalDatabase > port: 3306
externalDatabase > user: bn_wordpress
ingress > enabled : true
ingress > hostname : wp.example.com

[root@lb-01 ~]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces
[root@lb-01 ~]# kubectl exec -it acid-zalando-postgres-operator-0 /bin/bash -n rancher-test-ns
kubectl port-forward service/acid-zalando-postgres-operator-pooler 5432:5432 -n rancher-test-ns
psql -h localhost -p 5432 -U postgres -d postgres
XjQhz5hvDneYmLnBfg96UXoeUltk4465W38SfFz7c08WZQKoBrYbdfCVb8AUA5Hm
GRANT ALL PRIVILEGES ON DATABASE "test_db" to test_pg_user;
\connect test_db
CREATE SCHEMA test_schema;
ALTER SCHEMA test_schema OWNER TO test_pg_user;


psql -h 192.168.0.160 -p 5432 -U test_pg_user -d test_db

[root@lb-01 ~]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces
[root@lb-01 ~]# kubectl -n rancher-test-ns logs -f wordpress-7657ff9867-bz6pk
[root@lb-01 ~]# kubectl -n rancher-test-ns logs -f acid-zalando-postgres-operator-0
[root@lb-01 ~]# kubectl -n rancher-test-ns describe pod wordpress-mariadb-0
[root@lb-01 ~]# kubectl -n rancher-test-ns describe acid-zalando-postgres-operator-0
[root@lb-01 ~]# kubectl exec -it rancher-6bcbdd6cb7-trsd9 /bin/bash -n cattle-system
[root@lb-01 ~]# kubectl exec -it apache-php-748fd847cc-jzp74 /bin/bash -n rancher-test-ns
[root@lb-01 ~]# kubectl exec -it mysql-innodbcluster-router-548db6bf46-zpw6d /bin/bash -n mysql-operator

[root@lb-01 ~]# kubectl exec -it wordpress-apache-7b644b87d6-q5v4x /bin/bash -n rancher-test-ns
[root@lb-01 ~]# kubectl exec -it "mysql-operator-0" /busybox/sh -n rancher-test-ns
kubectl get service mysql-operator -n rancher-test-ns
kubectl describe service mysql-operator -n rancher-test-ns

[root@lb-01 ~]# kubectl port-forward service/mysql-operator prometheus -n rancher-test-ns
[root@lb-01 ~]# mysqlsh -h127.0.0.1 -P6446 -uroot -p

[root@lb-01 ~]# kubectl exec -it `kubectl get pod -n rancher-test-ns | grep apache-php | awk -F " " '{print $1}'` /bin/bash -n rancher-test-ns  

[root@lb-01 ~]# kubectl port-forward mysql-innodbcluster-0 6446:6446 -n mysql-operator






## WinSCPで接続するためのトンネルコマンド（コマンドプロンプトで実行）
ssh root@192.168.0.150 "kubectl port-forward `kubectl get pod -n rancher-test-ns | grep apache-php | awk '{print $1}'` 10022:22 -n rancher-test-ns"

## PostgreSQLで接続するためのトンネルコマンド（コマンドプロンプトで実行）
ssh root@192.168.0.150 -L 5432:localhost:5432 "ps -ef | grep acid-zalando-postgres-operator | grep -v grep | xargs kill -9; kubectl port-forward `kubectl get pods -o jsonpath={.items..metadata.name} -l application=spilo -n rancher-test-ns` 5432:5432 -n rancher-test-ns"

## MySQLのorchestrator Web UIのトンネルコマンド（コマンドプロンプトで実行）
ssh root@192.168.0.150 -L 80:localhost:8080 "kubectl port-forward service/mysql-operator 8080:80 -n mysql-operator"

## MySQLで接続するためのトンネルコマンド（コマンドプロンプトで実行）
ssh root@192.168.0.150 -L 3306:localhost:3306 "kubectl port-forward service/mysql-cluster-db-mysql 3306:3306 -n mysql-operator"

## kubernetesのpodログを確認
[root@lb-01 ~]# kubectl logs apache-php-799c6dc65f-d2qzr -n rancher-test-ns | less

## 1つのpodの状態詳細確認
[root@lb-01 ~]# kubectl describe pods apache-php-799c6dc65f-d2qzr -n rancher-test-ns | less
[root@lb-01 ~]# kubectl describe ingress apache-php-799c6dc65f-d2qzr -n rancher-test-ns


postgres-operator作成

vi ./laravel-ddd-sample-psql-pv.yaml
```
kind: PersistentVolume
apiVersion: v1
metadata:
  name: laravel-ddd-sample-psql-pv
spec:
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: hostpath
  hostPath:
    path: /home
```
kubectl apply -f ./laravel-ddd-sample-psql-pv.yaml -n rancher-test-ns


vi ./postgresql.yaml
```
kind: "postgresql"
apiVersion: "acid.zalan.do/v1"

metadata:
  name: "acid-laravel-ddd-sample-psql9"
  namespace: "rancher-test-ns"
  labels:
    team: acid

spec:
  teamId: "acid"
  postgresql:
    version: "14"
  numberOfInstances: 1
  enableMasterLoadBalancer: true
  enableReplicaLoadBalancer: true
  enableConnectionPooler: true
  volume:
    size: "10Gi"
    storageClass: hostpath
  users:
    test_pg_user: []
  databases:
    test_db: test_pg_user
  allowedSourceRanges:
    # IP ranges to access your cluster go here
  
  resources:
    requests:
      cpu: 100m
      memory: 100Mi
    limits:
      cpu: 500m
      memory: 500Mi
```
kubectl apply -f ./postgresql.yaml -n rancher-test-ns

[root@lb-01 ~]# kubectl get pvc pgdata-acid-laravel-ddd-sample-psql4-0 -n rancher-test-ns -o yaml > ./postgres-operator-pvc.yaml
[root@lb-01 ~]# vi ./postgres-operator-pvc.yaml
※「  volumeMode: Filesystem」の下に以下を追記
```
  volumeName: laravel-ddd-sample-psql-pv
```
[root@lb-01 ~]# kubectl apply -f ./postgres-operator-pvc.yaml -n rancher-test-ns

[root@lb-01 ~]# export PGMASTER=$(kubectl get pods -o jsonpath={.items..metadata.name} -l application=spilo -n rancher-test-ns); echo $PGMASTER
[root@lb-01 ~]# kubectl get secret postgres.acid-laravel-ddd-sample-psql9.credentials.postgresql.acid.zalan.do -o 'jsonpath={.data.password}' -n rancher-test-ns | base64 -d

pooler
Erkk45GASO0zXBpYccXtLJTAYki1hMhPudVCy85Sv85sBJHWjd62KQtLbWxOolWI

postgres
XjQhz5hvDneYmLnBfg96UXoeUltk4465W38SfFz7c08WZQKoBrYbdfCVb8AUA5Hm

standby
0VVRCfpST7vMcuzLQ9Es6gG2QLtc52rpVN0CuGPMehyl8tvCzHzDOepK9oEXayOc

test_pg_user
6nmLZkaGvU5bBSMQHkG8paMRBXwoN6fs4sdwscJSpTDGSYt8eb165AkGUW9pLfYS

[root@lb-01 ~]# kubectl port-forward $PGMASTER 5432:5432 -n rancher-test-ns &
[root@lb-01 ~]# psql -U postgres -h localhost -p 55432


# Apps & Marketplaceで以下を追加
Name:zalando-postgres-operator
http Index URL:https://raw.githubusercontent.com/zalando/postgres-operator/master/charts/postgres-operator

# 試したいこと
・worker nodeの追加
・PostgreSQL Operator
・Longhorn




# _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/
# _/ iptables legacy
[root@rancher-master-01 ~]# cp /etc/yum.repos.d/Rocky-Sources.repo /etc/yum.repos.d/Rocky-Sources.repo_org
[root@rancher-master-01 ~]# vi /etc/yum.repos.d/Rocky-Sources.repo
```diff
[baseos-source]
-#mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=BaseOS-$releasever-source
+#mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=BaseOS-$releasever-source
+baseurl=https://dl.rockylinux.org/pub/rocky/8/BaseOS/source/tree/

[appstream-source]
-mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=AppStream-$releasever-source
+#mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=AppStream-$releasever-source
+baseurl=https://dl.rockylinux.org/pub/rocky/8/AppStream/source/tree/

-#[extras-source]
+[extras-source]
#name=Rocky Linux $releasever - Extras - Source
#mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=source&repo=extras-$releasever-source
#baseurl=http://dl.rockylinux.org/$contentdir/$releasever/extras/source/tree/
#gpgcheck=1
#enabled=0
#gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial
+name=Rocky Linux $releasever - Extras - Source
+baseurl=https://dl.rockylinux.org/pub/rocky/8/extras/source/tree/
+gpgcheck=1
+enabled=0
+gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rockyofficial
```
[root@rancher-master-01 ~]# cp /etc/yum.repos.d/Rocky-Sources.repo /etc/yum.repos.d/Rocky-Sources.repo_legacy
[root@rancher-master-01 ~]# cp /etc/yum.repos.d/Rocky-PowerTools.repo /etc/yum.repos.d/Rocky-PowerTools.repo_org
[root@rancher-master-01 ~]# vi /etc/yum.repos.d/Rocky-PowerTools.repo
```diff
-enabled=0
+enabled=1
```

[root@rancher-master-01 ~]# cp /etc/yum.repos.d/Rocky-PowerTools.repo /etc/yum.repos.d/Rocky-PowerTools.repo_legacy
[root@rancher-master-01 ~]# dnf update -y

[root@rancher-master-01 ~]# dnf groupinstall -y "RPM Development Tools" "Development Tools"
[root@rancher-master-01 ~]# dnf install -y libpcap-devel libselinux-devel libmnl-devel libnetfilter_conntrack-devel libnfnetlink-devel libnftnl
[root@rancher-master-01 ~]# dnf install -y yum-utils
[root@rancher-master-01 ~]# cd ~/
[root@rancher-master-01 ~]# yumdownloader --source libnftnl
[root@rancher-master-01 ~]# rpm -ivh libnftnl-1.1.5-5.el8.src.rpm
[root@rancher-master-01 ~]# cd ~/rpmbuild/SPECS/
[root@rancher-master-01 ~]# rpmbuild -bb libnftnl.spec
[root@rancher-master-01 ~]# rpm -ivh ~/rpmbuild/RPMS/x86_64/libnftnl-devel-1.1.5-5.el8.x86_64.rpm
[root@rancher-master-01 ~]# yumdownloader --source iptables
[root@rancher-master-01 ~]# rpm -ivh iptables-1.8.4-24.el8.src.rpm
[root@rancher-master-01 ~]# cd ~/rpmbuild/SPECS/
[root@rancher-master-01 ~]# cp iptables.spec iptables.spec_org
[root@rancher-master-01 ~]# vi iptables.spec

```diff
＜20行目＞
-Release: 24%{?dist}
+Release: 24_include_legacy%{?dist}

＜353行目＞
# drop all legacy tools
-rm -f %{buildroot}%{_sbindir}/*legacy*
-rm -f %{buildroot}%{_bindir}/iptables-xml
-rm -f %{buildroot}%{_mandir}/man1/iptables-xml*
-rm -f %{buildroot}%{_mandir}/man8/xtables-legacy*
+#rm -f %{buildroot}%{_sbindir}/*legacy*
+#rm -f %{buildroot}%{_bindir}/iptables-xml
+#rm -f %{buildroot}%{_mandir}/man1/iptables-xml*
+#rm -f %{buildroot}%{_mandir}/man8/xtables-legacy*

＜461行目＞
%{_libdir}/xtables/libarpt*
%{_libdir}/xtables/libebt*
%{_libdir}/xtables/libipt*
%{_libdir}/xtables/libip6t*
%{_libdir}/xtables/libxt*
+%{_sbindir}/*legacy*
+%{_bindir}/iptables-xml
+%doc %{_mandir}/man1/iptables-xml*
+%doc %{_mandir}/man8/xtables-legacy*
```

[root@rancher-master-01 ~]# cp iptables.spec iptables.spec_legacy
[root@rancher-master-01 ~]# rpmbuild -bb iptables.spec
[root@rancher-master-01 ~]# rpm -Fvh ~/rpmbuild/RPMS/x86_64/iptables-*
[root@rancher-master-01 ~]# ln -nfs xtables-legacy-multi /usr/sbin/iptables
[root@rancher-master-01 ~]# iptables -V
[root@rancher-master-01 ~]# vi /etc/yum.conf
```diff
+exclude=iptables*
```

[root@rancher-master-01 ~]# rpm -evh libnftnl-devel


[root@rancher-master-01 ~]# systemctl list-unit-files -t service | grep firewalld
[root@rancher-master-01 ~]# systemctl stop firewalld
[root@rancher-master-01 ~]# systemctl disable firewalld
[root@rancher-master-01 ~]# systemctl list-unit-files -t service | grep nftables
[root@rancher-master-01 ~]# systemctl enable nftables
[root@rancher-master-01 ~]# systemctl start nftables
[root@rancher-master-01 ~]# cp /etc/nftables/main.nft  /etc/nftables/main.nft_org
[root@rancher-master-01 ~]# cp /etc/sysconfig/nftables.conf /etc/sysconfig/nftables.conf_org
[root@rancher-master-01 ~]# vi /etc/sysconfig/nftables.conf
```
※以下を一番下へ追記
include "/etc/nftables/rancher.nft"
```

[root@rancher-master-01 ~]# touch /etc/nftables/rancher.nft
[root@rancher-master-01 ~]# vi /etc/nftables/rancher.nft
```
#!/usr/bin/nft -f

# drop any existing nftables ruleset
# flush ruleset

table ip filter {
        set safeip {
                type ipv4_addr
                flags interval
                elements = {
                        192.168.0.0/24,
                        10.42.0.0/15,
                }
        }
        chain INPUT {
                type filter hook input priority filter; policy accept;
                ct state related,established accept
                #ip protocol icmp accept
                #iifname "enp2s0" accept

                tcp dport 80 accept
                tcp dport 179 accept
                tcp dport 443 accept
                tcp dport 2379 accept
                tcp dport 2380-2381 accept
                tcp dport 4240 accept
                tcp dport 5473 accept
                tcp dport 6443 accept
                tcp dport 8080-8081 accept
                tcp dport 9090 accept
                tcp dport 9093-9094 accept
                tcp dport 9098 accept
                tcp dport 9099 accept
                tcp dport 9345 accept
                tcp dport 9443 accept
                tcp dport 9796 accept
                tcp dport 10249 accept
                tcp dport 10250-10252 accept
                tcp dport 10257 accept
                tcp dport 10259 accept
                tcp dport 30000-32767 accept
                udp dport 4789 accept
                udp dport 8472 accept
                udp dport 51820-51821 accept
                counter packets 5 bytes 248 log prefix "[nftables INPUT DROP] : "
        }
}
```
[root@rancher-master-01 ~]# systemctl restart nftables.service
[root@rancher-master-01 ~]# vi /etc/rsyslog.conf
```diff
+#
+# Include all config files in /etc/rsyslog.d/
+#
+$IncludeConfig /etc/rsyslog.d/*.conf
```

[root@rancher-master-01 ~]# vi /etc/rsyslog.d/10-nftables.conf
```
:msg, contains, "[nftables" -/var/log/nftables-deny.log
& ~
```

[root@rancher-master-01 ~]# systemctl restart rsyslog
[root@rancher-master-01 ~]# touch /var/log/nftables-deny.log
[root@rancher-master-01 ~]# less /var/log/nftables-deny.log
[root@rancher-master-01 ~]# nft list ruleset






# 以下Oracleのmysql operatorインストール手順（使い物にならなかった）

Name : mysql-operator
URL : https://mysql.github.io/mysql-operator/
Type : https

## mysql-operator
Namespace : mysql-operator
Name : mysql-operator
Version : 2.0.4

## mysql-innodbcluster
Namespace : mysql-operator
Name : mysql-innodbcluster
Version : 2.0.4
credentials.root.host : '%'
credentials.root.user : 'root'
credentials.root.password : 'ffff'
credentials.wp_user.host : '%'
credentials.wp_user.user : 'wp_user'
credentials.wp_user.password : 'wp_pass'
routerInstances : 1
serverInstances : 2


[root@lb-01 ~]# helm repo add mysql-operator https://mysql.github.io/mysql-operator/
[root@lb-01 ~]# helm install mysql-operator mysql-operator/mysql-operator --namespace mysql-operator --create-namespace

### ダメな場合
[root@lb-01 ~]# mkdir ~/mysql-operator
[root@lb-01 ~]# cd ~/mysql-operator
[root@lb-01 ~]# git clone https://github.com/mysql/mysql-operator.git .
[root@lb-01 ~]# helm install mysql-operator ./helm/mysql-operator --namespace mysql-operator --create-namespace

### ダメで削除
[root@lb-01 ~]# helm uninstall mysql-operator --namespace mysql-operator
[root@lb-01 ~]# helm uninstall mysql-innodbcluster --namespace mysql-operator

#### ※force delete
kubectl patch InnoDBCluster mysql-innodbcluster -p '{"metadata":{"finalizers":[]}}' --type='merge' -n mysql-operator
kubectl delete InnoDBCluster mysql-innodbcluster --grace-period=0 --force --namespace mysql-operator

kubectl patch pod mysql-innodbcluster-0 -p '{"metadata":{"finalizers":[]}}' --type='merge' -n mysql-operator
kubectl delete pod mysql-innodbcluster-0 --grace-period=0 --force --namespace mysql-operator

NAME: mysql-operator
LAST DEPLOYED: Mon Apr 11 20:16:00 2022
NAMESPACE: mysql-operator
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Create an InnoDB Cluster by executing:
1. When using a source distribution / git clone: `helm install [cluster-name] -n [ns-name] ~/helm/mysql-innodbcluster`
2. When using Helm repos :  `helm install [cluster-name] -n [ns-name] mysql-innodbcluster`
[root@lb-01 ~]# helm install mycluster mysql-operator/mysql-innodbcluster \
  --set credentials.root.user='root' \
  --set credentials.root.password='ffff' \
  --set credentials.root.host='%'\
  --set serverInstances=3 \
  --set routerInstances=1 \
  --set tls.useSelfSigned=true

[root@lb-01 ~]# helm install mysql-innodbcluster helm/mysql-innodbcluster --namespace mysql-operator \
  --set credentials.root.user='root' \
  --set credentials.root.password='ffff' \
  --set credentials.root.host='%' \
  --set serverInstances=3 \
  --set routerInstances=1

### ダメな場合
[root@lb-01 ~]# mkdir ~/mysql-operator
[root@lb-01 ~]# cd ~/mysql-operator
[root@lb-01 ~]# git clone https://github.com/mysql/mysql-operator.git .
[root@lb-01 ~]# helm install mysql-innodbcluster ./helm/mysql-innodbcluster --namespace mysql-operator \
  --set credentials.root.user='root' \
  --set credentials.root.password='ffff' \
  --set credentials.root.host='%' \
  --set credentials.wp_user.user='wp_user' \
  --set credentials.wp_user.password='wp_pass' \
  --set credentials.wp_user.host='%' \
  --set serverInstances=3 \
  --set routerInstances=1 \
  --set tls.useSelfSigned=true
[root@lb-01 ~]# kubectl get deployment -n mysql-operator mysql-operator
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
mysql-operator   1/1     1            1           12m

### UPGRADE（更新・アップデート）
[root@lb-01 ~]# cd ~/mysql-operator
[root@lb-01 ~]# helm upgrade mysql-innodbcluster ./helm/mysql-innodbcluster \
  --namespace mysql-operator \
  --set credentials.root.user='root' \
  --set credentials.root.password='ffff' \
  --set credentials.root.host='%' \
  --set serverInstances=2 \
  --set routerInstances=2

[root@lb-01 ~]# kubectl get services -n mysql-operator
NAME                            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                               AGE
mysql-innodbcluster             ClusterIP   10.43.242.103   <none>        6446/TCP,6448/TCP,6447/TCP,6449/TCP   13m
mysql-innodbcluster-instances   ClusterIP   None            <none>        3306/TCP,33060/TCP,33061/TCP          13m
mysql-operator                  ClusterIP   10.43.52.242    <none>        9443/TCP                              14m

[root@lb-01 ~]# kubectl describe service mysql-innodbcluster -n mysql-operator
Name:              mysql-innodbcluster
Namespace:         mysql-operator
Labels:            app.kubernetes.io/managed-by=Helm
                   mysql.oracle.com/cluster=mysql-innodbcluster
                   tier=mysql
Annotations:       <none>
Selector:          component=mysqlrouter,mysql.oracle.com/cluster=mysql-innodbcluster,tier=mysql
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.43.242.103
IPs:               10.43.242.103
Port:              mysql  6446/TCP
TargetPort:        6446/TCP
Endpoints:         10.42.140.225:6446
Port:              mysqlx  6448/TCP
TargetPort:        6448/TCP
Endpoints:         10.42.140.225:6448
Port:              mysql-ro  6447/TCP
TargetPort:        6447/TCP
Endpoints:         10.42.140.225:6447
Port:              mysqlx-ro  6449/TCP
TargetPort:        6449/TCP
Endpoints:         10.42.140.225:6449
Session Affinity:  None
Events:            <none>

[root@lb-01 ~]# kubectl port-forward service/mysql-innodbcluster mysql -n mysql-operator
[root@lb-01 ~]# mysqlsh -h127.0.0.1 -P6446 -uroot -p
MySQL  127.0.0.1:6446 ssl  JS > \sql
MySQL  127.0.0.1:6446 ssl  SQL > CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
MySQL  127.0.0.1:6446 ssl  SQL > CREATE USER 'wp_user'@'%' IDENTIFIED BY 'wp_pass';
MySQL  127.0.0.1:6446 ssl  SQL > GRANT ALL PRIVILEGES ON wordpress.* to 'wp_user'@'%';
MySQL  127.0.0.1:6446 ssl  SQL > FLUSH PRIVILEGES;
MySQL  127.0.0.1:6446 ssl  SQL > SELECT User, Host, Plugin FROM mysql.user;
MySQL  127.0.0.1:6446 ssl  SQL > ALTER USER 'wp_user'@'%' IDENTIFIED WITH mysql_native_password BY 'wp_pass';
MySQL  127.0.0.1:6446 ssl  SQL > ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'ffff';

[root@lb-01 ~]# kubectl get nodes -o wide; kubectl get pods -o wide --all-namespaces
[root@lb-01 ~]# kubectl exec -it apache-php-748fd847cc-pkjm9 /bin/bash -n rancher-test-ns
[root@apache-php-5d4bcbc86b-zbzv8 /]# yum install -y mysql
[root@apache-php-5d4bcbc86b-zbzv8 /]# mysql -P 6446 --host=mysql-innodbcluster.mysql-operator --user=wp_user --password=wp_pass wordpress

http://laravel-ddd-sample/blog
### ※以下インストール時設定
データベース名 : wordpress
db : wordpress
ユーザー名 : wp_user
パスワード : wp_pass
データベースのホスト名(ポート番号6446も入れること！) : mysql-innodbcluster.mysql-operator:6446
テーブル接頭辞 : wp_
サイトのタイトル : wordpress-test
ユーザ名 : tadanobu
パスワード : dqEnGV0WTXXZzBnP!P
メールアドレス : hatake.t14@gmail.com
検索エンジンでの表示 : 検索エンジンがサイトをインデックスしないようにするチェックON

## MySQLで接続するためのトンネルコマンド（コマンドプロンプトで実行）
ssh root@192.168.0.150 "kubectl port-forward service/mysql-operator prometheus -n rancher-test-ns"







## Prometheus

### Monitoring

```
ingressNginx:
  serviceMonitor:
    interval: 300s

prometheus:
  prometheusSpec:
    evaluationInterval: 5m
    scrapeInterval: 5m

```

### Receivers

spec:
  name: alert
  email_configs:
    - send_resolved: true
      require_tls: true
      to: hatake.t14@gmail.com
      from: hatake.t14@gmail.com
      auth_username: hatake.t14@gmail.com
      smarthost: smtp.gmail.com:587
      auth_password: xxxxxxxxxxxxxxx
      tls_config: {}
  kind: Secret
    creationTimestamp: '2022-07-04T13:58:30Z'
    fields:
      - alertmanager-rancher-monitoring-alertmanager
    labels:
      app: rancher-monitoring-alertmanager
      release: rancher-monitoring
    name: alertmanager-rancher-monitoring-alertmanager
    namespace: cattle-monitoring-system

### Routes

spec:
  receiver: alert
  group_by:
    - namespace
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 4h
  match:
    {}
  match_re:
    alertname: ^Pgpool.*$
  name: route-1

spec:
  receiver: alert
  group_by:
    - namespace
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 4h
  match:
    alertname: RancherTestNsTargetDown
  match_re:
    {}
  name: route-2

spec:
  receiver: alert
  group_by:
    - namespace
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 4h
  match:
    {}
  match_re:
    alertname: ^MySQL.*$
  name: route-3

spec:
  receiver: alert
  group_by:
    - namespace
  group_wait: 30s
  group_interval: 5m
  repeat_interval: 4h
  match:
    {}
  match_re:
    alertname: ^Postgresql.*$
  name: route-4


### PrometheusRule

apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  annotations:
    prometheus-operator-validated: "true"
  creationTimestamp: "2022-07-08T11:09:17Z"
  generation: 5
  managedFields:
  - apiVersion: monitoring.coreos.com/v1
    fieldsType: FieldsV1
    fieldsV1:
      f:spec:
        .: {}
        f:groups: {}
    manager: rancher
    operation: Update
    time: "2022-07-08T11:09:17Z"
  name: rancher-test-ns-prometheus-rule
  namespace: rancher-test-ns
  resourceVersion: "19568755"
  uid: b9b89340-3ccc-49cd-9cfc-be9886a72c33
spec:
  groups:
  - interval: 300s
    name: rancher-test-ns-alive-monitoring
    rules:
    - alert: RancherTestNsTargetDown
      annotations:
        message: namespace rancher-test-ns pod target-down.
      expr: up{container="apache"} == 0
      for: 300s
      labels:
        severity: critical


apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  annotations:
    prometheus-operator-validated: "true"
  creationTimestamp: "2022-07-14T12:58:01Z"
  generation: 8
  managedFields:
  - apiVersion: monitoring.coreos.com/v1
    fieldsType: FieldsV1
    fieldsV1:
      f:spec:
        .: {}
        f:groups: {}
    manager: rancher
    operation: Update
    time: "2022-07-14T12:58:01Z"
  name: pgpool-prometheus-rule
  namespace: rancher-test-ns
  resourceVersion: "20413229"
  uid: 67e50114-e81e-43ad-9abe-69d21d8abcc1
spec:
  groups:
  - interval: 300s
    name: pgpool-alive-monitoring
    rules:
    - alert: PgpoolProcessDown
      annotations:
        message: pgpool process down
      expr: |-
        0 == sum(
        pgpool2_backend_used_ratio{container="pgpool", endpoint="pgpool-stats", job="postgresql-port", namespace="rancher-test-ns", service="postgresql-port"})
      for: 300s
      labels:
        severity: critical
    - alert: PgpoolTargetDown
      annotations:
        message: pgpool target down
      expr: 0 == pgpool2_up
      for: 300s
      labels:
        severity: critical
    - alert: PgpoolCpuRate
      annotations:
        message: pgpoolcpu rate over 0.8
      expr: 0.8 < rate(container_cpu_usage_seconds_total{container="pgpool", cpu="total", namespace="rancher-test-ns"}[5m])
      for: 300s
      labels:
        severity: critical


apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  annotations:
    prometheus-operator-validated: "true"
  creationTimestamp: "2022-07-17T01:24:54Z"
  generation: 9
  managedFields:
  - apiVersion: monitoring.coreos.com/v1
    fieldsType: FieldsV1
    fieldsV1:
      f:spec:
        .: {}
        f:groups: {}
    manager: rancher
    operation: Update
    time: "2022-07-17T01:24:54Z"
  name: postgresql-prometheus-rule
  namespace: rancher-test-ns
  resourceVersion: "20413929"
  uid: 88da1464-60a3-442d-b636-248e77963e87
spec:
  groups:
  - interval: 300s
    name: postgresql-alive-monitoring
    rules:
    - alert: PostgresqlTargetDown
      annotations:
        message: PostgreSQL target down.
      expr: 0 == pg_up
      for: 300s
      labels:
        severity: critical
    - alert: PostgresqlManyConnect
      annotations:
        message: postgresql many connection.
      expr: sum(pg_stat_activity_count) >= sum(pg_settings_max_connections)
      for: 300s
      labels:
        severity: critical


apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  annotations:
    prometheus-operator-validated: "true"
  creationTimestamp: "2022-07-17T13:04:42Z"
  generation: 2
  managedFields:
  - apiVersion: monitoring.coreos.com/v1
    fieldsType: FieldsV1
    fieldsV1:
      f:spec:
        .: {}
        f:groups: {}
    manager: rancher
    operation: Update
    time: "2022-07-17T13:04:41Z"
  name: mysql-alive-monitoring
  namespace: rancher-test-ns
  resourceVersion: "19652103"
  uid: 2c196705-d449-4000-95d2-106a89a6b788
spec:
  groups:
  - interval: 300s
    name: MySQLProcessDown
    rules:
    - alert: MySQLTargetDown
      annotations:
        message: MySQL process down
      expr: 0 == mysql_up
      for: 300s
      labels:
        severity: critical




## GitLab（Registries）
### ※参考サイト
### https://dev.classmethod.jp/articles/deploy-the-gitlab-on-eks-with-helm/
### https://gitlab.com/gitlab-org/charts/gitlab/-/issues/727

Name : gitlab
URL : http://charts.gitlab.io/
Type : https

### GitLab（Install App）
Namespace : gitlab-ns
Name : gitlab
Version : 6.2.0

### values

#### values(SSL Version)
$ sudo mkdir -p /etc/ssl/rdev.gitlab.com 
$ sudo bash -c "openssl req -x509 -sha256 -newkey rsa:2048 -days 3650 -nodes -subj \"/C=JP/ST=Hyogo/L=Kobe City/O=Hatake Co., Ltd/OU=System/CN=*.rdev.gitlab.com\" -extensions SAN -config <( cat /etc/ssl/openssl.cnf <(printf \"[SAN]\nsubjectAltName='DNS:*.rdev.gitlab.com'\")) -out /etc/ssl/rdev.gitlab.com/server.crt -keyout /etc/ssl/rdev.gitlab.com/server.key" 
$ sudo bash -c "openssl rsa -text < /etc/ssl/rdev.gitlab.com/server.key" 
$ sudo bash -c "openssl x509 -text < /etc/ssl/rdev.gitlab.com/server.crt" 

kubectl create secret tls gitlab-tls --cert=./server.crt --key=./server.key -n gitlab-ns 

certmanager:
  install: false
certmanager-issuer:
  email: hatake.t14@gmail.com
gitlab:
  gitlab-shell:
    minReplicas: 1
    maxReplicas: 1
  gitlab-pages:
    hpa:
      maxReplicas: 1
      minReplicas: 1
  kas:
    minReplicas: 1
    maxReplicas: 1
  mailroom:
    hpa:
      minReplicas: 1
      maxReplicas: 1
  sidekiq:
    minReplicas: 1
    maxReplicas: 1
  spamcheck:
    hpa:
      maxReplicas: 1
      minReplicas: 1
  webservice:
    minReplicas: 1
    maxReplicas: 1
registry:
  hpa:
    minReplicas: 1
    maxReplicas: 1
gitlab-runner:
  install: false
global:
  edition: ce
  controller:
    replicaCount: 1
  hosts:
    domain: rdev.gitlab.com
  ingress:
    configureCertmanager: false
    tls: 
      secretName: 'gitlab-tls'
nginx-ingress:
  controller:
    service:
      externalTrafficPolicy: false
      type: ClusterIP
    autoscaling:
      minReplicas: 1
      maxReplicas: 1
    keda:
      minReplicas: 1
      maxReplicas: 1
  defaultBackend:
    autoscaling:
      minReplicas: 1
      maxReplicas: 1

#### values(No SSL Version)
certmanager:
  install: false
certmanager-issuer:
  email: hatake.t14@gmail.com
gitlab:
  gitlab-shell:
    minReplicas: 1
    maxReplicas: 1
  gitlab-pages:
    hpa:
      maxReplicas: 1
      minReplicas: 1
  kas:
    minReplicas: 1
    maxReplicas: 1
  mailroom:
    hpa:
      minReplicas: 1
      maxReplicas: 1
  sidekiq:
    minReplicas: 1
    maxReplicas: 1
  spamcheck:
    hpa:
      maxReplicas: 1
      minReplicas: 1
  webservice:
    minReplicas: 1
    maxReplicas: 1
registry:
  hpa:
    minReplicas: 1
    maxReplicas: 1
gitlab-runner:
  install: false
global:
  edition: ce
  controller:
    replicaCount: 1
  hosts:
    domain: example.com
    gitlab:
      https: false
    https: false
    kas: 
      https: false
    minio:
      https: false
    pages: 
      https: false
    registry:
      https: false
  ingress:
    annotations:
      nginx.ingress.kubernetes.io/force-ssl-redirect: "false"
      nginx.ingress.kubernetes.io/ssl-redirect: "false"
    configureCertmanager: false
    tls:
      enabled: false
nginx-ingress:
  controller:
    service:
      externalTrafficPolicy: false
      type: ClusterIP
    hostNetwork: true
    kind: DaemonSet
    autoscaling:
      minReplicas: 1
      maxReplicas: 1
    keda:
      minReplicas: 1
      maxReplicas: 1
  defaultBackend:
    autoscaling:
      minReplicas: 1
      maxReplicas: 1

## レジストリのテスト
tadanobu@Yukiko-HP:/$ sudo vi /etc/docker/daemon.json
{
  "insecure-registries" : ["registry.rdev.gitlab.com","registry.rdev.gitlab.com:443","registry.rdev.gitlab.com:4567"]
}
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker stop
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker start
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker status
tadanobu@Yukiko-HP:/$ sudo /usr/sbin/service docker start; docker login registry.example.com -u root -p NI4Mmtrrn9UBDnIOSkFQebuWvMT4zRb7izOyrtz6PucKoNxIBd9rCar2xIOkxn0G
tadanobu@Yukiko-HP:/$ cd /mnt/c/k8s/rancher-catalog/apache-php/Docker; docker build -t registry.example.com/root/test/apache-php:latest .; docker push registry.example.com/root/test/apache-php:latest
tadanobu@Yukiko-HP:Docker$ cd /mnt/c/k8s/rancher-catalog; git add .; git commit . -m "apache-php更新"; git push -u origin main












## 以下MySQL Operator(Bitpoke Operator for MySQL)インストール手順（使い物にならなかった）

Name : helm-charts.bitpoke.io
URL : https://helm-charts.bitpoke.io
Type : https

### mysql-operator
Namespace : mysql-operator
Name : mysql-operator
version :0.6.2
replicaCount : 2

### mysql-cluster
Namespace : mysql-operator
Name : mysql-cluster
version :0.6.2
appDatabase: wordpress
appPassword: wp_pass
appUser: wp_user
replicaCount : 2
rootPassword: ffff

[root@apache-php-5d4bcbc86b-zbzv8 /]# mysql -P 3306 --host=mysql-cluster-db-mysql.mysql-operator --user=root --password=ffff

### ※以下インストール時設定
データベース名 : wordpress
db : wordpress
ユーザー名 : wp_user
パスワード : wp_pass
データベースのホスト名(ポート番号6446も入れること！) : mysql-cluster-db-mysql.mysql-operator:3306
テーブル接頭辞 : wp_
サイトのタイトル : wordpress-test
ユーザ名 : tadanobu
パスワード : y9rk2qbdz4fvhza5ZK
メールアドレス : hatake.t14@gmail.com
検索エンジンでの表示 : 検索エンジンがサイトをインデックスしないようにするチェックON


### Pgpool-II-PostgreSQL  
#### スレーブ(物理サーバのプライマリをレプリケーションする想定)
Namespace : pgpool-postgresql
Name : pgpool-postgresql
version :0.0.169

affinity: 
  - 'rancher-master-01'

env:
  POSTGRES_REPLICATION_MODE: 'slave'
  POSTGRESQL_HOT_STANDBY: 'on'

#### スレーブ(rancherのプライマリをレプリケーションする想定)
Namespace : pgpool-postgresql
Name : pgpool-postgresql-s
version :0.0.169

affinity: 
  - 'rancher-master-01'

env:
  POSTGRES_REPLICATION_MODE: 'slave'
  POSTGRESQL_HOT_STANDBY: 'on'
  POSTGRES_MASTER_PORT_NUMBER: '30000'
  POSTGRESQL_MASTER_HOST: 192.168.0.151

global:
  pgpool:
    nodePort: 30011
    statsNodePort: 30012
  postgresql:
    nodePort: 30010
    statsNodePort: 30013


kubectl exec -it `kubectl get pod -n apache-php | grep apache-php | grep Running | awk -F " " '{print $1}'` /bin/bash -n apache-php --container apache  






rm -rf /etc/firewalld/direct.xml

firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 1 -s 127.0.0.1/32 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 2 -d 127.0.0.1/32 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 3 -m state --state ESTABLISHED,RELATED -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter OUTPUT 4 -m state --state ESTABLISHED,RELATED -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 5 -o cali+ -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 6 -i cali+ -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 7 -p all -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 20 -p tcp -i enp2s0 -s 192.168.0.0/24 -d 192.168.0.0/24 dport 80 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 21 -p tcp -i enp2s0 -s 192.168.0.0/24 -d 192.168.0.0/24 --dport 443 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 50 -p tcp --dport 9345 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 51 -p tcp --dport 6443 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 52 -p udp --dport 8472 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 53 -p tcp --dport 10250 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 54 -p tcp --dport 2379 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 55 -p tcp --dport 2380 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 56 -p tcp --dport 30000:32767 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 57 -p udp --dport 8472 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 58 -p tcp --dport 4240 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 59 -p tcp --dport 179 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 60 -p udp --dport 4789 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 61 -p tcp --dport 5473 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 62 -p tcp --dport 9098 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 63 -p tcp --dport 9099 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 64 -p tcp --dport 5473 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 65 -p udp --dport 8472 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 66 -p tcp --dport 9099 -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 67 -p udp --dport 51820:51821 -j ACCEPT

cat /etc/firewalld/direct.xml

systemctl restart firewalld.service
less /var/log/firewalld





dnf install -y iptables-services
iptables -L > /tmp/itables_before







dnf install -y nftables
systemctl start nftables.service
cp /etc/nftables/main.nft  /etc/nftables/main.nft_org
cp /etc/sysconfig/nftables.conf /etc/sysconfig/nftables.conf_org

vi /etc/sysconfig/nftables.conf
```
※以下を一番下へ追記
include "/etc/nftables/rancher.nft"
```

cp /etc/nftables/main.nft /etc/nftables/rancher.nft
vi /etc/nftables/rancher.nft
```
# drop any existing nftables ruleset
# flush ruleset

add table ip filter
add rule ip filter INPUT tcp dport 9345 counter accept
add rule ip filter INPUT tcp dport 6443 counter accept
add rule ip filter INPUT udp dport 8472 counter accept
add rule ip filter INPUT tcp dport 10250 counter accept
add rule ip filter INPUT tcp dport 2379 counter accept
add rule ip filter INPUT tcp dport 2380 counter accept
add rule ip filter INPUT tcp dport 30000-32767 counter accept
add rule ip filter INPUT udp dport 8472 counter accept
add rule ip filter INPUT tcp dport 4240 counter accept
add rule ip filter INPUT tcp dport 179 counter accept
add rule ip filter INPUT udp dport 4789 counter accept
add rule ip filter INPUT tcp dport 5473 counter accept
add rule ip filter INPUT tcp dport 9098 counter accept
add rule ip filter INPUT tcp dport 9099 counter accept
add rule ip filter INPUT tcp dport 5473 counter accept
add rule ip filter INPUT udp dport 8472 counter accept
add rule ip filter INPUT tcp dport 9099 counter accept
add rule ip filter INPUT udp dport 51820-51821 counter accept
```

nft list ruleset
```
table ip filter {
        chain INPUT {
                type filter hook input priority filter; policy accept;
                tcp dport 9345 counter packets 0 bytes 0 accept
                tcp dport 6443 counter packets 7016 bytes 683296 accept
                udp dport 8472 counter packets 0 bytes 0 accept
                tcp dport 10250 counter packets 250 bytes 22036 accept
                tcp dport 2379 counter packets 13191 bytes 1628023 accept
                tcp dport 2380 counter packets 0 bytes 0 accept
                tcp dport 30000-32767 counter packets 0 bytes 0 accept
                udp dport 8472 counter packets 0 bytes 0 accept
                tcp dport 4240 counter packets 0 bytes 0 accept
                tcp dport 179 counter packets 6 bytes 369 accept
                udp dport 4789 counter packets 0 bytes 0 accept
                tcp dport 5473 counter packets 0 bytes 0 accept
                tcp dport 9098 counter packets 0 bytes 0 accept
                tcp dport 9099 counter packets 108 bytes 7623 accept
                tcp dport 5473 counter packets 0 bytes 0 accept
                udp dport 8472 counter packets 0 bytes 0 accept
                tcp dport 9099 counter packets 0 bytes 0 accept
                udp dport 51820-51821 counter packets 0 bytes 0 accept
        }
}
```

https://www.bit-hive.com/articles/20211006
https://thinca.hatenablog.com/entry/nftables-settings-memo-2020
https://kusoneko.blogspot.com/2019/11/centos-8-nftables.html

vi /etc/nftables/rancher.nft
```
table ip filter {
        set safeip {
                type ipv4_addr
                flags interval
                elements = {
                        192.168.0.0/24,
                        10.42.0.0/15,
                }
        }
        chain INPUT {
                type filter hook input priority filter; policy accept;
                ct state related,established accept
                #ip protocol icmp accept
                #iifname "enp2s0" accept

                tcp dport 9345 accept
                tcp dport 6443 accept
                udp dport 8472 accept
                tcp dport 10250 accept
                tcp dport 2379 accept
                tcp dport 2380 accept
                tcp dport 30000-32767 accept
                udp dport 8472 accept
                tcp dport 4240 accept
                tcp dport 179 accept
                udp dport 4789 accept
                tcp dport 5473 accept
                tcp dport 9098 accept
                tcp dport 9099 accept
                tcp dport 5473 accept
                udp dport 8472 accept
                tcp dport 9099 accept
                udp dport 51820-51821 accept
                tcp dport 9796 accept
                tcp dport 10250-10252 accept
                counter packets 5 bytes 248 log prefix "[nftables INPUT DROP] : "
        }
}
```
systemctl start nftables.service

less /var/log/firewalld-deny.log

nft list ruleset




# PostgreSQLストリーミングレプリケーションのフェイルオーバー手順

1	【確認OK】セカンダリダウン時にプライマリが影響がないかを確認
ダウン中・・・参照可、更新可
ダウンから復帰・・・参照可、更新可、セカンダリ同期OK

2	【確認OK】svr5-dbダウン⇒待機サーバーのDBをプライマリへ昇格
https://changineer.info/server/postgresql/postgresql_streaming_replication03.html
https://www.fujitsu.com/jp/products/software/resources/feature-stories/postgres/article-index/streaming-replication2/

2-1.	svr5-dbのPgpool-II、PostgreSQL停止（障害を想定した停止）
[root@svr5-db ]# 	systemctl stop pgpool.service 
[root@svr5-db ]# 	systemctl stop postgresql-14.service 

2-2.	待機サーバーのDBをマスターへ昇格

2-2-1.	Rancherでpgpool-postgresqlの設定変更。
	env:
	  POSTGRES_REPLICATION_MODE: master
	  POSTGRESQL_HOT_STANDBY: 'off'

2-2-2.	Rancherでpgpool-postgresql > postgresqlのpodへ接続し、セカンダリ→プライマリへ昇格する。	
	bash-4.4$ 	rm -rf /var/lib/pgsql/.user_conf_initialized
	bash-4.4$ 	tail -n 1000 /var/lib/pgsql/data/log/postgresql-Fri.log | grep 'selected new timeline'
	bash-4.4$ 	/usr/pgsql-14/bin/pg_ctl promote -D /var/lib/pgsql/data
	bash-4.4$ 	ps aux | grep /usr/pgsql-14/bin/postgres | grep -v grep | awk '{ print "kill -9", $2 }' | sh
	bash-4.4$ 	tail -n 1000 /var/lib/pgsql/data/log/postgresql-Fri.log | grep 'selected new timeline'
		※プライマリへの昇格を確認（「selected new timeline ID: 」と表示されたらOK）
	bash-4.4$ 	ls -la /var/lib/pgsql/data/pg_wal
		※タイムラインIDが、上記確認で表示された「selected new timeline ID: 」の次の数字と同じWALが出力されるようになっている事を確認
		
2-2-3.	Rancherでpgpool-postgresql > pgpoolのpodへ接続し、PostgreSQLのプロセスをkill -termする。	
	bash-4.4$ 	ps aux | grep /usr/bin/pgpool | grep -v grep | awk '{ print "kill -TERM", $2 }' | sh

2-3.	svr5-dbをスレーブへ降格	
	[root@svr5-db ]# 	rm -rf /var/lib/pgsql/14/data
	[root@svr5-db ]# 	su postgres
	bash-4.4$ 	pg_basebackup -D /var/lib/pgsql/14/data -U repl -h 192.168.0.151 -p 32432 -X stream -w -v -P -R
	bash-4.4$ 	vim /var/lib/pgsql/14/data/postgresql.conf
		※以下のようになっている事を確認（なっていなければ変更する）
		hot_standby = on
		
		less /var/lib/pgsql/14/data/postgresql.auto.conf
		※192.168.0.151をマスターとしてレプリケーションする設定になっている事を確認
		
	[postgres@svr5-db ]$ 	exit
	[root@svr5-db ]# 	systemctl restart postgresql-14.service 
	[root@svr5-db ]# 	systemctl start pgpool.service 

2-4.	動作確認（Rancher）	
	bash-4.4$ 	psql -h 127.0.0.1 -U postgres
	postgres=# 	\x
	postgres=# 	SELECT * FROM pg_stat_replication;

  ※以下の内容であることを確認					
  usename		repl			
  application_name		walreceiver			
  client_addr		192.168.0.160		　→　192.168.0.151でもOK	
  state		streaming			
  sync_priority		1			
  sync_state		sync			
  ※待機サーバーでDBデータを更新したら、svr5-dbへレプリケーションされる事を確認

3	【確認OK】svr5-dbをスレーブ⇒プライマリへ復帰させる		
	3-1.	待機サーバーのDBを停止	
		※Rancherでex-pgpool-postgresqlを削除（PresistentVolumesが消えるまで確認（ブラウザリロードして確認する））。	
			
	3-2.	svr5-dbをスレーブ⇒マスターへ昇格	
		[root@svr5-db ]# 	less /var/lib/pgsql/14/data/postgresql.conf
			※以下になっている事を確認
			synchronous_standby_names = 'walreceiver'
			
		[root@svr5-db ]# 	vim /var/lib/pgsql/14/data/postgresql.conf
			※以下のようになっている事を確認（なっていなければ変更する）
			hot_standby = off
			
		[root@svr5-db ]# 	mv /var/lib/pgsql/14/data/postgresql.auto.conf /var/lib/pgsql/14/data/postgresql.auto.conf_back
		[root@svr5-db ]# 	su postgres
		[postgres@svr5-db ]$ 	/usr/pgsql-14/bin/pg_ctl promote -D /var/lib/pgsql/14/data
		[postgres@svr5-db ]$ 	tail -n 10 /var/lib/pgsql/14/data/log/postgresql-Fri.log | grep 'selected new timeline'
			※プライマリへの昇格を確認（「selected new timeline ID: 」と表示されたらOK）
		[postgres@svr5-db ]$ 	ls -la /var/lib/pgsql/14/data/pg_wal
			※タイムラインIDが、上記確認で表示された「selected new timeline ID: 」の次の数字と同じWALが出力されるようになっている事を確認
		[root@svr5-db ]# 	systemctl restart postgresql-14.service 
		[root@svr5-db ]# 	systemctl start pgpool.service 
			
			
	3-3.	待機サーバーをマスター⇒スレーブへ降格	
		※Rancherでex-pgpool-postgresqlを削除（PresistentVolumesが消えるまで確認（ブラウザリロードして確認する））。	
			
		[root@svr-standby ]# 	rm -rf /var/lib/pgsql
			
		※Rancherでex-pgpool-postgresqlを以下Valuesの指定でインストールする。	
		env:	
		  POSTGRES_REPLICATION_MODE: slave	
		  POSTGRESQL_HOT_STANDBY: 'on'	
			
	3-4.	動作確認	
		[root@svr5-db ]# 	psql -h 127.0.0.1 -U postgres
		postgres=# 	\x
		postgres=# 	SELECT * FROM pg_stat_replication;

    ※以下の内容であることを確認			
    usename		repl	
    application_name		walreceiver	
    client_addr		192.168.0.151	
    state		streaming	
    sync_priority		1	
    sync_state		sync	

		※svr5-dbでDBデータを更新したら、待機サーバーへレプリケーションされる事を確認
		
4	svr5-dbのpostgresql.confやpg_hba.confを調整	
	待機サーバーのレプリケーションで設定が変わっている箇所があるので、必要であれば元通りの設定へ調整する。	
